import click
import time


@click.group()
def cli():
    '''Welcome the to cli for managing Server'''
    pass


@click.command()
def initdb():
    from server import engine
    from thegridBack.models import  createTables
    createTables(engine)
    click.echo('Initialized the database')


@click.command()
def dropdb():
    from server import engine
    from thegridBack.models import destroyTables
    destroyTables(engine)
    click.echo('Dropped the database')


@click.command()
def testdb():
    from test.populate_disease import populate_disease, populate_test, populate_skill
    from test.populate_users import create_dummy_doctor_data, create_dummy_user_data, create_dummy_lab_data
    from test.fillinfo import fill_docskill, fill_labtest, fill_schedule, fill_all_schedule
    populate_disease()
    populate_test()
    populate_skill()
    create_dummy_lab_data()
    create_dummy_user_data()
    create_dummy_doctor_data()
    click.echo('Dummy data inserted')
    time.sleep(2)
    fill_labtest()
    fill_docskill()
    fill_schedule()
    fill_all_schedule()
    click.echo('Relationship tables filled')


cli.add_command(initdb)
cli.add_command(dropdb)
cli.add_command(testdb)


if __name__ == '__main__':
    cli()
