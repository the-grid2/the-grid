# The idea is to save a PASS in .env file which is not access by other users
# Then if a user ask for migration he have to give that PASS.
from datetime import datetime
import click
import os, getpass

from thegridBack.models import Migrator
from thegridBack.config import tzone

# Password validation from the .env
def validate_pass(ctx, param, value):
    if value == os.environ.get('PASS'):
        return value
    else:
        print('Password does not match', end=' ', flush=True)
        ctx.abort()


def add_action(user):
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    session.add(user)
    print('\n[+] Your action applied successfully: {}'.format(user.action))
    session.commit()
    session.close()
    connection.close()


@click.group()
@click.option('--Password', prompt=True, callback=validate_pass, hide_input=True)
def cli(password):
    """ Welcome to the cli to manage database actions """
    pass


@click.command()
def init():
    ''' Create a Directory for migrations '''

    if os.system('python migrations.py db init > /dev/null 2>&1'):
        print('[-] The Directory migration already Exist.')
    else:
        print('[+] Directory name Migration has been created.')


@click.command()
@click.argument('message')
def migrate(message):
    ''' Create a revision file for database '''

    user = getpass.getuser()

    # The systemcall return the exit status of the command, and also show the command output but not return it. 
    
    if os.system('python migrations.py db migrate'):
        print('[-] Revision identifier from "alembic_version" table does not match.')
        print('[-] Missing last migration version from the version directory.')
    else:
        user_ = Migrator(
            user=user,
            action='Migrate',
            message=message,
            action_on=datetime.utcnow()+tzone
        )
        add_action(user_)


@click.command()
@click.option('-r', '--revision', default='head')
@click.argument('message')
def upgrade(revision, message):
    ''' Upgrade Database by the revision file '''

    user = getpass.getuser()
    if os.system('python migrations.py db upgrade {}'.format(revision)):
        print('[-] Cannot locate revision identifier.')
    else:
        user_ = Migrator(
            user=user,
            action='upgrade',
            message=message,
            revision=revision,
            action_on=datetime.utcnow()+tzone  
        )
        add_action(user_)


@click.command()
@click.option('-r', '--revision', default='')
@click.argument('message')
def downgrade(revision, message):
    ''' Downgrade Database to last version '''

    user = getpass.getuser()
    if os.system('python migrations.py db downgrade {}'.format(revision)):
        print('[-] Unable to downgrade.')
    else:
        user_ = Migrator(
            user=user,
            action='downgrade',
            revision='revision',
            message=message,
            action_on=datetime.utcnow()+tzone
        )
        add_action(user_)


@click.command()
def revision():
    ''' Create a revision file for manual operation '''

    if os.system('python migrations.py db revision '):
        print('[-] Unable to take revision of the identifier.')
    else:
        print('[+] Successfully created revision file.')


cli.add_command(migrate)
cli.add_command(init)
cli.add_command(upgrade)
cli.add_command(downgrade)
cli.add_command(revision)


if __name__ == '__main__':
    cli()