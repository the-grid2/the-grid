import uuid
import datetime
import random

sex=['female', 'male']


def create_dummy_user_data():
    from server import SQLSession
    from thegridBack.models import Address, User
    session = SQLSession()
    connection = session.connection()
    # add 20 dummy user
    for i in range(1, 10):
        adrs = Address(
            address_line_1="W-16, Road No 2 for-{}".format(str(i)),
            locality='Nai Sunhari',
            landmark='opp Khanna Rice mill',
            city='Kichha',
            state='Uttarakhand',
            zip_code='263148',
            lat=28.905563+i/1000,
            lng=79.506921+i/10000
        )

        u = User(
            public_id=str(uuid.uuid4()),
            email="testUser{}@grid.in".format(str(i)),
            username="test user{}".format(str(i)),
            password="abcd",
            contact_number="91{}3760849".format(str(i-1)),
            uidai="52542631781245{}6".format(i),
            age=random.randint(14, 56),
            gender=sex[random.randint(0,1)],
            registered_on=datetime.datetime.utcnow(),
            last_updated_on=datetime.datetime.utcnow(),
            varified=True
        )
        u.address.append(adrs)
        session.add(u)
    session.commit()
    session.close()
    connection.close()


def create_dummy_doctor_data():
    from server import SQLSession
    from thegridBack.models import Address, Doctor
    session = SQLSession()
    connection = session.connection()
    # add 20 dummy user
    for i in range(1, 10):
        adrs = Address(
            address_line_1="W-18, Road No 5 for-{}".format(str(i)),
            locality='Kichha',
            landmark='opp Mill',
            city='Kichha',
            state='Uttarakhand',
            zip_code='263148',
            lat=28.905563+i/1000,
            lng=79.506921+i/10000
        )

        u = Doctor(
            public_id=str(uuid.uuid4()),
            email="testDoctor{}@grid.in".format(str(i)),
            doctorname="test doctor{}".format(i),
            password="abcd",
            contact_number="91{}3760749".format(str(i-1)),
            uidai="52542631781245{}6".format(i),
            age=random.randint(14, 56),
            gender=sex[random.randint(0,1)],
            registration_number=456+i,
            registered_on=datetime.datetime.utcnow(),
            last_updated_on=datetime.datetime.utcnow(),
        )
        u.address.append(adrs)
        session.add(u)
    session.commit()
    session.close()
    connection.close()


def create_dummy_lab_data():
    from server import SQLSession
    from thegridBack.models import Address, DiagnoseLab
    session = SQLSession()
    connection = session.connection()
    # add 20 dummy user
    for i in range(1, 10):
        adrs = Address(
            address_line_1="W-19, Road No 6 for-{}".format(str(i)),
            locality='Kichha',
            landmark='opp',
            city='Kichha',
            state='Uttarakhand',
            zip_code='263148',
            lat=28.905563+i/1000,
            lng=79.506921+i/10000
        )

        u = DiagnoseLab(
            labname="test lab{}".format(str(i)),
            contact_number="91{}3760649".format(str(i-1)),
            registration_number=567+i-1,
            registered_on=datetime.datetime.utcnow(),
            last_updated_on=datetime.datetime.utcnow(),
        )
        u.address.append(adrs)
        session.add(u)
    session.commit()
    session.close()
    connection.close()