import json


def populate_disease():
    path = './dummy_data/disease.json'
    print(path)
    fp = open(path,)
    data = json.load(fp)
    from server import SQLSession
    from thegridBack.models import Disease, DiseaseDetails
    session = SQLSession()
    connection = session.connection()
    # print(data)
    for key in data['Sheet1']:
        symp = ""
        for i in key['Symptoms']:
            symp+=i
            symp+=", "
        pres = ""
        for i in key['Prescriptions']:
            pres+=i
            pres+=", "
        disease = Disease(
            diseasename=key['Disease'],
            symptoms=symp
        )
        disease_detail = DiseaseDetails(
            description=key['Description'],
            prescription=pres
        )
        print(key['Disease'])
        disease.disease_details.append(disease_detail)
        session.add(disease)
    session.commit()
    session.close()
    connection.close()

def populate_test():
    path = './dummy_data/testsavail.json'
    print(path)
    fp = open(path,)
    data = json.load(fp)
    from server import SQLSession
    from thegridBack.models import TestAvail, Speciality
    session = SQLSession()
    connection = session.connection()
    for i in data['Tests']:
        tst = TestAvail(
            testname=i
        )
        session.add(tst)
        print(i)
    session.commit()
    session.close()
    connection.close()

def populate_skill():
    path = './dummy_data/speciality.json'
    print(path)
    fp = open(path,)
    data = json.load(fp)
    from server import SQLSession
    from thegridBack.models import Speciality
    session = SQLSession()
    connection = session.connection()
    for i in data['Speciality']:
        skl = Speciality(
            skill=i
        )
        session.add(skl)
        print(i)
    session.commit()
    session.close()
    connection.close()
