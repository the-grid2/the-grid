import random
import json


def fill_labtest():
    from server import SQLSession
    from thegridBack.models import LabTest
    session = SQLSession()
    connection = session.connection()
    for i in range(1, 10):
        for j in range(1, 94):
            lt = LabTest(
                price = random.randint(100, 1200),
                diagnoselab_id=i,
                testavail_id=j
            )
            session.add(lt)
    session.commit()
    session.close()
    connection.close()


def fill_docskill():
    from server import SQLSession
    from thegridBack.models import DocSpecial
    session = SQLSession()
    connection = session.connection()
    for i in range(1, 10):
        for j in range(1, 57):
            ds = DocSpecial(
                price = random.randint(100, 1200),
                doctor_id=i,
                speciality_id=j
            )
            session.add(ds)
    session.commit()
    session.close()
    connection.close()


def fill_schedule():
    from server import SQLSession
    from thegridBack.models import LabSchedule, DoctorSchedule, Schedule
    session = SQLSession()
    connection = session.connection()
    path = './dummy_data/timeslots.json'
    print(path)
    fp = open(path,)
    data = json.load(fp)
    for i in data['Time slots']:
        sc = Schedule(
            weekday=i['weekday'],
            start_time=i['start_time'],
            end_time=i['end_time']
        )
        session.add(sc)
    session.commit()
    session.close()
    connection.close()


def fill_all_schedule():
    from server import SQLSession
    from thegridBack.models import Schedule, DoctorSchedule, LabSchedule
    session = SQLSession()
    connection = session.connection()
    for i in range(1, 10):
        for j in range(i*random.randint(1, 5), random.randint(45, 54)):
            ds = DoctorSchedule(
                schedule_id=j,
                doctor_id=i
            )
            session.add(ds)
    for i in range(1, 10):
        for j in range(i*random.randint(1, 5), random.randint(45, 54)):
            ls = LabSchedule(
                schedule_id=j,
                diagnoselab_id=i
            )
            session.add(ls)
    session.commit()
    session.close()
    connection.close()
