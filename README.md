# The-Grid

## Backend app for the-grid

## Setup instructions

### Server

- #### Step 1 (optional but recommended)

  Create a python virtual environment by using virtualenv or conda

  ```bash
  conda create -n environment python3.6
  ```

  or

  ```bash
  python -m venv environment && source venv/bin/activate
  ```

- #### Step 2

  Clone this repo

  ```bash
  git clone <repo> && cd <name>
  ```

- #### Step 4

  Install Docker and Docker-Compose in your system

  ```bash
  docker-compose up --build --remove-orphans
  ```

## Backend runs at ->             http://localhost:5000
## API documentation serves at -> http://localhost:8080
## Frontend runs at ->            http://localhost:80
