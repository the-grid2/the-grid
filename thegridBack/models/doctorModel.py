from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, Float, DDL, event, Index
from sqlalchemy.orm import relationship
from .meta import Base
from flask_bcrypt import Bcrypt

from thegridBack.models.tsvector import TsVector

flask_bcrypt = Bcrypt()


class Doctor(Base):
    """Doctor Model for storing doctor related details """
    __tablename__ = "doctor"

    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String(255), unique=True, nullable=False)
    public_id = Column(String(100), unique=True)
    doctorname = Column(String(127), nullable=False)
    password_hash = Column(String(100), nullable=False)
    contact_number = Column(String(10), nullable=False, unique=True)
    uidai = Column(String(24), nullable=False, unique=True)
    age = Column(Integer, nullable=False)
    gender = Column(String(12), nullable=False)
    registration_number = Column(Integer, nullable=False, unique=True)

    email_varified = Column(Boolean, nullable=False, default=False)
    reg_no_varified =Column(Boolean, nullable=False, default=False)
    registered_on = Column(DateTime, nullable=False)
    last_updated_on = Column(DateTime, nullable=False)

    #Relationships begin
    address = relationship("Address", back_populates="doctor")
    appointment = relationship("Appointment", back_populates="doctor")
    doc_rating = relationship("DocRating", back_populates="doctor")
    health_history = relationship("HealthHistory", back_populates="doctor")
    doctor_schedule = relationship("DoctorSchedule", back_populates="doctor")
    docspecial = relationship("DocSpecial", back_populates="doctor")

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = flask_bcrypt.generate_password_hash(
            password).decode('utf-8')

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<DoctorName '{}'>\n<Email '{}'>".format(self.doctorname, self.email)


class Speciality(Base):
    """Specialization model to store specialization of doctor"""
    __tablename__ = 'speciality'

    id = Column(Integer, primary_key=True, autoincrement=True)
    skill = Column(String, nullable=False)
    skill_tsvector = Column(TsVector)
    __table_args__ = (Index('skill_tsvector_id', 'skill_tsvector', postgresql_using = 'gin'),)

    # Relationship begins
    docspecial = relationship("DocSpecial", back_populates="speciality")


# Creating trigger for Speciality table
trigger_snippet = DDL("""
    CREATE TRIGGER skill_tsvector_update BEFORE INSERT OR UPDATE
    ON speciality
    FOR EACH ROW EXECUTE PROCEDURE
    tsvector_update_trigger(skill_tsvector, 'pg_catalog.english', 'skill')
""")
event.listen(Speciality.__table__, 'after_create', trigger_snippet.execute_if(dialect = 'postgresql'))



class DocSpecial(Base):
    """Association table for doctor and specialization relation"""
    __tablename__ = "docspecial"

    id = Column(Integer, primary_key=True, autoincrement=True)
    price = Column(Float, nullable=False)

    # Foreignkey and relationship
    doctor_id = Column(Integer, ForeignKey('doctor.id'))
    doctor = relationship("Doctor", back_populates="docspecial")
    speciality_id = Column(Integer, ForeignKey('speciality.id'))
    speciality = relationship("Speciality", back_populates="docspecial")


class DocRating(Base):
    """ DocRate Table to store rating of the doctor """
    __tablename__ = "doc_rating"

    id = Column(Integer, primary_key=True, autoincrement=True)
    rate = Column(Float, nullable=False)
    comment = Column(String)

    # Foreign Key and relationship
    doctor_id = Column(Integer, ForeignKey('doctor.id'))
    doctor = relationship("Doctor", back_populates="doc_rating")
