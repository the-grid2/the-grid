from .meta import Base
from .addressModel import Address
from .appointModel import Appointment, AppointResult
from .diseaseModel import Disease, DiseaseDetails
from .doctorModel import DocSpecial, Doctor, Speciality, DocRating
from .diagnoselabModel import DiagnoseLab, LabTest, TestAvail
from .scheduleModel import Schedule, DoctorSchedule, LabSchedule
from .userModel import User, HealthHistory
from .migratorModel import Migrator
from .otpModel import Otp
from .tsvector import ComparatorFactory

def createTables(engine):
    # print("Binding")
    print(Base.metadata.tables.keys())
    Base.metadata.bind = engine
    # print("binding done")
    Base.metadata.create_all(engine)


def destroyTables(engine):
    Base.metadata.drop_all(engine)
