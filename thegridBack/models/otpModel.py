from sqlalchemy import Column, Integer, String, DateTime, Boolean
from .meta import Base
import datetime


class Otp(Base):
    '''For storing the otp'''
    __tablename__ = "otp"
    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String(255), unique=True, nullable=True)
    otp = Column(Integer, nullable=False)
    generated_time_stamp = Column(
        DateTime, nullable=False, default=datetime.datetime.utcnow())
    secret = Column(String(32), nullable=False)
    isValid = Column(Boolean, nullable=False, default=True)
