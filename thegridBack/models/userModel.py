from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from .meta import Base
from flask_bcrypt import Bcrypt

flask_bcrypt = Bcrypt()


class User(Base):
    """ User Model for storing user related details """
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String(255), unique=True, nullable=False)
    public_id = Column(String(255), unique=True)
    username = Column(String(255), nullable=False)
    password_hash = Column(String(127), nullable=False)
    contact_number = Column(String(10), nullable=False, unique=True)
    uidai = Column(String(24), nullable=False, unique=True)
    age = Column(Integer, nullable=False)
    gender = Column(String(12), nullable=False)

    varified = Column(Boolean, nullable=False, default=False)
    registered_on = Column(DateTime, nullable=False)
    last_updated_on = Column(DateTime, nullable=False)

    # relationships begin
    address = relationship("Address", back_populates="user", lazy="joined")
    appointment = relationship("Appointment", back_populates="user")
    health_history = relationship("HealthHistory", back_populates="user")

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = flask_bcrypt.generate_password_hash(
            password).decode('utf-8')

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User '{}'>\n<Email '{}'>".format(self.username, self.email)


class HealthHistory(Base):
    """Health_History Model to Store health history of user"""
    __tablename__ = "health_history"

    id = Column(Integer, primary_key=True, autoincrement=True)
    disease = Column(String(511), nullable=False)
    test_report = Column(String(1024))
    discription = Column(String(2047), nullable=False)
    date = Column(DateTime, nullable=False)

    # Foriegn Keys and relationship
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship("User", back_populates="health_history")
    diagnoselab_id = Column(Integer, ForeignKey('diagnoselab.id'))
    diagnoselab = relationship("DiagnoseLab", back_populates="health_history")
    doctor_id =Column(Integer, ForeignKey('doctor.id'))
    doctor = relationship("Doctor", back_populates="health_history")
