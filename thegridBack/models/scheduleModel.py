from sqlalchemy import Column, Integer, String, ForeignKey, Time
from sqlalchemy.orm import relationship
from .meta import Base


class Schedule(Base):
    """Schedule model to store the schedule"""
    __tablename__ = "schedule"

    id = Column(Integer, primary_key=True, autoincrement=True)
    weekday = Column(String, nullable=False)
    start_time = Column(Time, nullable=False)
    end_time = Column(Time, nullable=False)

    # Relationship begins
    doctor_schedule = relationship("DoctorSchedule", back_populates="schedule")
    lab_schedule = relationship("LabSchedule", back_populates="schedule")


class DoctorSchedule(Base):
    """Association table for schedule and doctor"""
    __tablename__ = "doctor_schedule"

    id = Column(Integer, primary_key=True, autoincrement=True)

    # Foreign key and relationships
    schedule_id = Column(Integer, ForeignKey('schedule.id'))
    schedule = relationship("Schedule", back_populates="doctor_schedule")
    doctor_id = Column(Integer, ForeignKey('doctor.id'))
    doctor = relationship("Doctor", back_populates="doctor_schedule")


class LabSchedule(Base):
    """Association table for schedule and doctor"""
    __tablename__ = "lab_schedule"

    id = Column(Integer, primary_key=True, autoincrement=True)

    # Foreign key and relationships
    schedule_id = Column(Integer, ForeignKey('schedule.id'))
    schedule = relationship("Schedule", back_populates="lab_schedule")
    diagnoselab_id = Column(Integer, ForeignKey('diagnoselab.id'))
    diagnoselab = relationship("DiagnoseLab", back_populates="lab_schedule")
