from sqlalchemy import Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from .meta import Base
from geopy.distance import geodesic

from thegridBack.constant.countries import Countries


def get_distance(x, lat, lng):
    print(x)
    print(lat, lng)
    return geodesic(x, (float(lat), float(lng))).km


class Address(Base):
    """Table to store the address of user, doctor and Labs."""
    __tablename__ = "address"

    id = Column(Integer, primary_key=True, autoincrement=True)
    care_of = Column(String(50), nullable=False, default='')
    # house numer, street name
    address_line_1 = Column(String(200))
    # Locaity name
    locality = Column(String(200))
    landmark = Column(String(100))
    city = Column(String(200))
    state = Column(String(200), nullable=False)
    country = Column(String(200), nullable=False, default=Countries.India)
    zip_code = Column(Integer, nullable=False)

    lat = Column(Float, nullable=True)
    lng = Column(Float, nullable=True)

    # relationships begins
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship("User", back_populates="address")
    doctor_id = Column(Integer, ForeignKey('doctor.id'))
    doctor = relationship("Doctor", back_populates="address")
    diagnoselab_id = Column(Integer, ForeignKey('diagnoselab.id'))
    diagnoselab = relationship("DiagnoseLab", back_populates="address")

    # methods
    def __repr__(self):
        dict_address = {
            "id": self.id,
            "care_of": self.care_of,
            "address_line_1": self.address_line_1,
            "locality": self.locality,
            "landmark": self.landmark,
            "city": self.city,
            "state": self.state,
            "country": self.country,
            "zip_code": self.zip_code,
            # "latitude": self.lat,
            # "longitude": self.lng
        }
        return str(dict_address)
