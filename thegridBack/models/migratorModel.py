from .meta import Base
from sqlalchemy import Column, Integer, String, DateTime


class Migrator(Base):
    """ Migrator Model for storing database action by author """
    __tablename__ = "migrator"

    id = Column(Integer, primary_key=True, autoincrement=True)
    user = Column(String(255), nullable=False)
    action = Column(String(32))
    message = Column(String(124))
    revision = Column(String(124))
    action_on = Column(DateTime, nullable=False)

    def __repr__(self):
        return "<User: '{}'\tAction taken: '{}'>".format(self.user, self.action)
