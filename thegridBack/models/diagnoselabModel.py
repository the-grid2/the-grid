from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, Float, Index, DDL, event
from sqlalchemy.orm import relationship
from .meta import Base

from thegridBack.models.tsvector import TsVector


class DiagnoseLab(Base):
    """DiagnoseLab model to store details of DiagnoseLab"""
    __tablename__ = "diagnoselab"

    id = Column(Integer, primary_key=True, autoincrement=True)
    labname = Column(String(127), nullable=False)
    contact_number = Column(String(10), nullable=False)
    registration_number = Column(Integer, nullable=False, unique=True)

    contact_varified = Column(Boolean, nullable=False, default=False)
    reg_no_varified =Column(Boolean, nullable=False, default=False)
    registered_on = Column(DateTime, nullable=False)
    last_updated_on = Column(DateTime, nullable=False)

    #Relationship Begins
    address = relationship("Address", back_populates="diagnoselab")
    appointment = relationship("Appointment", back_populates="diagnoselab")
    health_history = relationship("HealthHistory", back_populates="diagnoselab")
    lab_test = relationship("LabTest", back_populates="diagnoselab")
    lab_schedule = relationship("LabSchedule", back_populates="diagnoselab")


class TestAvail(Base):
    """ Test available table to store all the available test """
    __tablename__ = "test_avail"

    id = Column(Integer, primary_key=True, autoincrement=True)
    testname = Column(String, nullable=False)
    testname_tsvector = Column(TsVector)
    __table_args__ = (Index('testname_tsvector_id', 'testname_tsvector', postgresql_using = 'gin'),)

    # Relationship begins
    lab_test = relationship("LabTest", back_populates="testavail")


# Creating trigger for TestAvail table
trigger_snippet = DDL("""
    CREATE TRIGGER testname_tsvector_update BEFORE INSERT OR UPDATE
    ON test_avail
    FOR EACH ROW EXECUTE PROCEDURE
    tsvector_update_trigger(testname_tsvector, 'pg_catalog.english', 'testname')
""")
event.listen(TestAvail.__table__, 'after_create', trigger_snippet.execute_if(dialect = 'postgresql'))



class LabTest(Base):
    """ Association table for diagnoselab and the test avail in it """
    __tablename__ = "lab_test"

    id = Column(Integer, primary_key=True, autoincrement=True)
    price = Column(Float, nullable=False)

    # Foreign key and relatioship
    diagnoselab_id = Column(Integer, ForeignKey('diagnoselab.id'), nullable=False)
    diagnoselab = relationship("DiagnoseLab", back_populates="lab_test")
    testavail_id = Column(Integer, ForeignKey('test_avail.id'), nullable=False)
    testavail = relationship("TestAvail", back_populates="lab_test")
