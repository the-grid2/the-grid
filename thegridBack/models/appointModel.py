from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, Float, Date, Time
from sqlalchemy.orm import relationship
from .meta import Base
from flask_bcrypt import Bcrypt

flask_bcrypt = Bcrypt()


class Appointment(Base):
    """Appointment Model for storing Appointment related details """
    __tablename__ = "appointment"

    id = Column(Integer, primary_key=True, autoincrement=True)
    date_created = Column(Date, nullable=False)

    start_time = Column(Time, nullable=False)
    aggenda = Column(String, nullable=False)
    done = Column(Boolean, default=False)
    canceled = Column(Boolean, default=False)
    price = Column(Float)

    # Foreigne keys and Relationship
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship("User", back_populates="appointment")
    doctor_id = Column(Integer, ForeignKey('doctor.id'))
    doctor = relationship("Doctor", back_populates="appointment")
    diagnoselab_id = Column(Integer, ForeignKey('diagnoselab.id'))
    diagnoselab = relationship("DiagnoseLab", back_populates="appointment")
    
    # Relatonships Begins
    appoint_result = relationship("AppointResult", back_populates="appointment")


class AppointResult(Base):
    """Appointment Result Tabel to Store the result of appointment"""
    __tablename__ = "appoint_result"

    id = Column(Integer, primary_key=True, autoincrement=True)
    disease = Column(String)
    conclusion = Column(String, nullable=False)

    # Foreign keys and relatonships
    appointment_id = Column(Integer, ForeignKey('appointment.id'))
    appointment = relationship("Appointment", back_populates="appoint_result")
