from sqlalchemy import Column, Integer, String, ForeignKey, Index, DDL, event
from sqlalchemy.orm import relationship
from .meta import Base

from thegridBack.models.tsvector import TsVector

class Disease(Base):
    """Table to store the disease and its symptoms."""
    __tablename__ = "disease"

    id = Column(Integer, primary_key=True, autoincrement=True)
    diseasename = Column(String, nullable=False, unique=True)
    symptoms = Column(String, nullable=False)
    symptoms_tsvector = Column(TsVector)
    __table_args__ =(Index('symptoms_tsvector_id', 'symptoms_tsvector', postgresql_using = 'gin'),)

    # Relationship Begins
    disease_details = relationship("DiseaseDetails", back_populates="disease")


# Creating trigger for symptoms table
trigger_snippet = DDL("""
    CREATE TRIGGER symptoms_tsvector_update BEFORE INSERT OR UPDATE
    ON disease
    FOR EACH ROW EXECUTE PROCEDURE
    tsvector_update_trigger(symptoms_tsvector, 'pg_catalog.english', 'symptoms')
""")
event.listen(Disease.__table__, 'after_create', trigger_snippet.execute_if(dialect = 'postgresql'))


class DiseaseDetails(Base):
    """ Table to storedetail of the disease"""
    __tablename__ = "disease_details"

    id = Column(Integer, autoincrement=True, primary_key=True)
    description = Column(String, nullable=False)
    prescription = Column(String)

    # Foreign Keys and relationship
    disease_id = Column(Integer, ForeignKey('disease.id'), unique=True)
    disease = relationship("Disease", back_populates="disease_details")
