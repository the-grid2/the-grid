from thegridBack.api.search import make_key
from flask import request, Blueprint
import uuid
import datetime

from thegridBack.response.response import make_response
from thegridBack.response.statusCodes import HTTPStatus
from thegridBack.auth.otpAuth import generateOtp, generateOtpToken
from thegridBack.auth import validate_token, get_token
from thegridBack.models import User, Address, Appointment
from thegridBack.constant import Constants

userBP = Blueprint('userApi', __name__)


@userBP.route('/register', methods=['POST'])
def useraction():
    if request.method == 'POST':
        data = request.json
        return save_new_user(data=data)
    else:
        return make_response("Method not allowed", HTTPStatus.MethodNotAllowed)


def save_new_user(data):
    required_parameters = ['email', 'name', 'password', 'contact_number', 'uidai', 'age', 'gender']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    from server import SQLSession
    session = SQLSession()
    connection = session.connection()

    _usr = session.query(User).filter_by(
        contact_number=data['contact_number']).first()
    if _usr:
        session.close()
        connection.close()
        return make_response("Contact already exist.", HTTPStatus.BadRequest)

    user = session.query(User).filter_by(email=data['email']).first()
    if user:
        session.close()
        connection.close()
        return make_response("User already exists", HTTPStatus.BadRequest)
    else:
        new_user = User(
            public_id=str(uuid.uuid4()),
            email=data['email'],
            username=data['name'].strip(),
            password=data['password'],
            contact_number=data['contact_number'],
            uidai=data['uidai'],
            age=data['age'],
            gender=data['gender'],
            registered_on=datetime.datetime.utcnow(),
            last_updated_on=datetime.datetime.utcnow()
        )
        otp = generateOtp(data['email'])
        otp_token = generateOtpToken(data['email'])
        # uncomment below two lines for sending otp on mail
        # if not send_confirmation_mail(data['name'], otp, data['email']):
        #     return make_response("Err sending mail, check your email id", HTTPStatus.InternalError)

        try:
            session.add(new_user)
            session.commit()
            session.close()
            connection.close()
            payload = {
                "token": otp_token,
                "email": data['email']
            }
            return make_response("User created successfully. otp sent to email", HTTPStatus.Success, payload)
        except Exception as e:
            session.close()
            connection.close()
            return make_response("Problem saving in db", HTTPStatus.InternalError)


@userBP.route('/login', methods=['POST'])
def login():
    data = request.json
    token = get_token(data)
    required_parameters = ['email', 'password']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    if token == Constants.NoUser:
        return make_response("User not exist", HTTPStatus.NotFound)
    elif token == Constants.NotVerified:
        return make_response("User not verified/blocked.", HTTPStatus.NotVerified)
    elif token == Constants.InvalidCredential:
        return make_response("Invalid Credential", HTTPStatus.UnAuthorised)
    else:
        res = {
            "email": data['email'],
            "auth-token": token
        }
        return make_response(msg="Token success", status_code=HTTPStatus.Success, payload=res)


@userBP.route('/', methods=['GET'])
def get_my_detail():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)

    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("User not verified", HTTPStatus.UnAuthorised)
    address = []
    for i in usr_.address:
        address.append({
            "id": 10,
            "care_of": '',
            "address_line_1": "W-16, Road No 2 for-1",
            "locality": "Nai Sunhari",
            "landmark": "opp Khanna Rice mill",
            "city": "Kichha",
            "state": "Uttarakhand",
            "country": "India",
            "zip_code": 263148
        })
    result = {
        "name": usr_.username,
        "varified": usr_.varified,
        "contact_number": usr_.contact_number,
        "email": usr_.email,
        "age": usr_.age,
        "gender": usr_.gender,
        # "address": [str(i) for i in usr_.address]
        "address": address[0]
    }
    return make_response(msg="Detail of the user", status_code=HTTPStatus.Success, payload=result)


@userBP.route('/change_password', methods=['POST'])
def change_password():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)
    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
    data = request.json
    from server import SQLSession
    session = SQLSession()
    required_parameters = ['old_password', 'new_password']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)
    connection = session.connection()
    u = session.query(User).filter_by(email=usr_.email).first()
    if u.check_password(data.get('old_password')):
        u.password = data['new_password']
        try:
            session.commit()
            session.close()
            connection.close()
            return make_response("Password change successful", HTTPStatus.Success)
        except:
            session.close()
            connection.close()
            return make_response("DB error", HTTPStatus.InternalError)
        finally:
            session.close()
            connection.close()
    else:
        session.close()
        connection.close()
        return make_response("Old password does not matched.", HTTPStatus.UnAuthorised)



@userBP.route('/update_profile', methods=['POST'])
def update_profile():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)
    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    data = request.json
    u = session.query(User).filter_by(email=usr_.email).first()
    # update the values if key to update is passed or leave it as same
    u.username = data['name'] if 'name' in data else u.username
    try:
        session.commit()
        session.close()
        connection.close()
        return make_response("Profile update successful", HTTPStatus.Success)
    except:
        session.close()
        connection.close()
        return make_response("DB error", HTTPStatus.InternalError)
    finally:
        session.close()
        connection.close()


@userBP.route('/add_address', methods=['POST'])
def add_user_address():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)
    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return "Invalid token", HTTPStatus.InvalidToken
        elif usr_ == Constants.TokenExpired:
            return "Token expired.", HTTPStatus.InvalidToken
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    data = request.json
    required_parameters = ['address_line_1', 'locality', 'landmark',
                            'city', 'state', 'country', 'zip_code', 'latitude', 'longitude', 'co']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    addr = Address(
        care_of=data['co'],
        address_line_1=data['address_line_1'],
        locality=data['locality'],
        landmark=data['landmark'],
        city=data['city'],
        state=data['state'],
        country=data['country'],
        zip_code=data['zip_code'],
        lat=data['latitude'],
        lng=data['longitude']
    )
    try:
        user = session.query(User).filter_by(id=usr_.id).first()
        user.address.append(addr)
        session.commit()
        session.close()
        connection.close()
        return make_response("Success adding address", HTTPStatus.Success)
    except:
        session.close()
        connection.close()
        return make_response("db error", HTTPStatus.InternalError)


@userBP.route('/get_address', methods=['GET'])
def get_my_address():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)
    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("Token error", HTTPStatus.UnAuthorised)
    try:
        address = [{
            "id": i.id,
            "co": i.care_of,
            "address_line_1": i.address_line_1,
            "locality": i.locality,
            "landmark": i.landmark,
            "city": i.city,
            "state": i.state,
            "country": i.country,
            "zip_code": i.zip_code,
            "latitude": i.lat,
            "longitude": i.lng
        }for i in usr_.address]
        return make_response("Array of address", HTTPStatus.Success, address)
    except:
        return make_response("Internal err in making response", HTTPStatus.InternalError)


@userBP.route('/remove_address', methods=['DELETE'])
def delete_a_address():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)
    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("Token error", HTTPStatus.UnAuthorised)
    if not usr_.varified:
        return make_response("Account not verified", HTTPStatus.UnAuthorised)

    address_id = request.args.get('address_id', None, type=int)
    if address_id == None:
        return make_response("Query parameter 'address_id' missing.", HTTPStatus.BadRequest)
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    try:
        adrs = session.query(Address).filter_by(
            id=address_id).filter_by(user_id=usr_.id).first()
        if not adrs:
            return make_response("No such Address for the user", HTTPStatus.BadRequest)
        session.delete(adrs)
        session.commit()
        session.close()
        connection.close()
        return make_response("Successfully deleted", HTTPStatus.Success)
    except:
        session.close()
        connection.close()
        return make_response("Internal Err", HTTPStatus.InternalError)


@userBP.route('/appointment', methods=['GET'])
def appointment():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)

    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("User not verified", HTTPStatus.UnAuthorised)

    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    appoint_q = session.query(Appointment).all()
    payload = []
    for i in appoint_q:
        if i.doctor_id != None:
            payload.append(
                {
                    "id": i.id,
                    "date_created": str(i.date_created),
                    "start_time": str(i.start_time),
                    "aggenda": i.aggenda,
                    "done": i.done,
                    "canceled": i.canceled,
                    "price": i.price,
                    "user_id": i.user_id,
                    "doctor_id": i.doctor_id,
                    "diagnoselab_id": i.diagnoselab_id,
                    "doctor_name": i.doctor.doctorname,
                    "doctor_number": i.doctor.contact_number
                }
            )
        else:
            payload.append(
                {
                    "id": i.id,
                    "date_created": str(i.date_created),
                    "start_time": str(i.start_time),
                    "aggenda": i.aggenda,
                    "done": i.done,
                    "canceled": i.canceled,
                    "price": i.price,
                    "user_id": i.user_id,
                    "doctor_id": i.doctor_id,
                    "diagnoselab_id": i.diagnoselab_id,
                    "lab_name": i.diagnoselab.labname,
                    "lab_contact_number": i.diagnoselab.contact_number
                }
            )
    
    return make_response("Success", HTTPStatus.Success, payload)
