import types
from thegridBack.models.diseaseModel import DiseaseDetails
from flask import Blueprint, request
from sqlalchemy import func

from thegridBack.response.statusCodes import HTTPStatus
from thegridBack.response.response import make_response
from thegridBack.models import Disease, Speciality, DocSpecial, LabTest, TestAvail
from thegridBack.utils.paginate import paginate_query


searchBP = Blueprint('searchApi', __name__)


@searchBP.route('/disease', methods=['GET'])
def disease():
    s_key = request.args.get('key', None, type=str)
    page = request.args.get('page', 1, type=int)
    if s_key == None:
        return make_response("Query parameter 'key' missing.", HTTPStatus.BadRequest)

    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    s_result = session.query(DiseaseDetails).join(Disease).filter(Disease.symptoms_tsvector.op('@@')(func.to_tsquery(s_key)))
    s_result = paginate_query(s_result, page=page)
    payload = [{
        "disease_id":i.disease.id,
        "diseasename":i.disease.diseasename,
        "symptoms":i.disease.symptoms,
        "description":i.description,
        "prescription":i.prescription

    } for i in s_result.items]
    session.close()
    connection.close()
    return make_response("success", HTTPStatus.Success, payload=payload)


@searchBP.route('/doctor', methods=['GET'])
def doctor():
    s_key = request.args.get('key', None, type=str)
    if s_key == None:
        return make_response("Query parameter 'key' missing.", HTTPStatus.BadRequest)
    
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    s_key = make_key(s_key)
    s_result = session.query(DocSpecial).join(Speciality).filter(Speciality.skill_tsvector.op('@@')(func.to_tsquery(s_key))).all()
    payload = [{
        "skill":i.speciality.skill,
        "doctorname":i.doctor.doctorname,
        "doctoremail":i.doctor.email,
        "doctor_id":i.doctor.id,
        "price":i.price,
    } for i in s_result]
    session.close()
    connection.close()
    return make_response("success", HTTPStatus.Success, payload=payload)


@searchBP.route('/diagnoselab', methods=['GET'])
def diagnoselab():
    s_key = request.args.get('key', None, type=str)
    if s_key == None:
        return make_response("Query parameter 'key' missing.", HTTPStatus.BadRequest)
    
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    s_key = make_key(s_key)
    s_result = session.query(LabTest).join(TestAvail).filter(TestAvail.testname_tsvector.op('@@')(func.to_tsquery(s_key))).all()
    payload = [{
        "testname":i.testavail.testname,
        "labname":i.diagnoselab.labname,
        "diagnoselab_id":i.diagnoselab.id,
        "contact_number":i.diagnoselab.contact_number,
        "price":i.price,
    } for i in s_result]
    session.close()
    connection.close()
    return make_response("success", HTTPStatus.Success, payload=payload)


def make_key(key):
    n_key = key.replace(" ", "\ ")
    return n_key
