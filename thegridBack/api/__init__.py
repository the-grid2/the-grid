from .user import userBP
from .search import searchBP
from .appointment import appointmentBP


API_VERSION_V1 = "v1"
