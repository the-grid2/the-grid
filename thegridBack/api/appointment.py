from thegridBack.models.diagnoselabModel import LabTest
from thegridBack.models.scheduleModel import LabSchedule
from flask import Blueprint, request
import datetime

from thegridBack.constant import Constants
from thegridBack.response.response import make_response
from thegridBack.auth import validate_token
from thegridBack.response.statusCodes import HTTPStatus
from thegridBack.models import DoctorSchedule, Schedule, Appointment, DocSpecial, Speciality, TestAvail

appointmentBP = Blueprint('appointApi', __name__)


@appointmentBP.route('/doc_slot', methods=['POST'])
def docslot():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)

    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("User not verified", HTTPStatus.UnAuthorised)
    data = request.json
    required_parameters = ['id', 'disease', 'date']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    doc_id = data['id']
    dt = data['date']
    year, month, day = (int(x) for x in dt.split('-'))    
    date = datetime.date(year, month, day)
    weekday = date.strftime("%A")
    weekday = weekday.lower()
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    all_slot = session.query(DoctorSchedule).join(Schedule).filter(
        DoctorSchedule.doctor_id==doc_id).filter(Schedule.weekday==weekday).all()
    booked_slot = session.query(Appointment).filter_by(doctor_id=doc_id).filter_by(date_created=date).all()
    booked = []
    for i in booked_slot:
        booked.append(i.start_time)
    avail_slot = []
    for i in all_slot:
        if i.schedule.start_time in booked:
            pass
        else:
            avail_slot.append({
                "start_time":str(i.schedule.start_time),
                "id":i.schedule.id
            })
    slot = [{
        "weekday": weekday,
        "time": i["start_time"],
        "schedule_id": i["id"]
    } for i in avail_slot]
    payload = {
        "doc_id": doc_id,
        "date": dt,
        "disease": data['disease'],
        "slot": slot
    }
    session.close()
    connection.close()
    return make_response("success", HTTPStatus.Success, payload)


@appointmentBP.route('/book_doc_slot', methods=['POST'])
def bookdocslot():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)

    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("User not verified", HTTPStatus.UnAuthorised)
    data = request.json
    required_parameters = ['doc_id', 'disease', 'date', 'schedule_id']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    doc_id = data['doc_id']
    schedule_id = data['schedule_id']
    dt = data['date']
    year, month, day = (int(x) for x in dt.split('-'))    
    date = datetime.date(year, month, day)
    weekday = date.strftime("%A")
    weekday = weekday.lower()
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    schedule_q = session.query(Schedule).filter_by(id=schedule_id).first()
    price_q = session.query(DocSpecial).join(Speciality).filter(Speciality.skill==data['disease']).filter(DocSpecial.doctor_id==doc_id).first()
    bk = Appointment(
        date_created = date,
        start_time = schedule_q.start_time,
        aggenda = data['disease'],
        price = price_q.price,
        doctor_id = doc_id
    )
    booking = {
        "date_created": date,
        "start_time":str(schedule_q.start_time),
        "aggenda": data['disease'],
        "price": price_q.price,
        "doctor_id": doc_id,
        "user_id": usr_.id
    }
    session.add(bk)
    session.commit()
    session.close()
    connection.close()
    return make_response("success", HTTPStatus.Success, booking)


@appointmentBP.route('/lab_slot', methods=['POST'])
def labslot():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)

    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("User not verified", HTTPStatus.UnAuthorised)
    data = request.json
    required_parameters = ['id', 'test', 'date']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    lab_id = data['id']
    dt = data['date']
    year, month, day = (int(x) for x in dt.split('-'))
    date = datetime.date(year, month, day)
    weekday = date.strftime("%A")
    weekday = weekday.lower()
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    all_slot = session.query(LabSchedule).join(Schedule).filter(
        LabSchedule.diagnoselab_id==lab_id).filter(Schedule.weekday==weekday).all()
    booked_slot = session.query(Appointment).filter_by(diagnoselab_id=lab_id).filter_by(date_created=date).all()
    booked = []
    for i in booked_slot:
        booked.append(i.start_time)
    avail_slot = []
    for i in all_slot:
        if i.schedule.start_time in booked:
            pass
        else:
            avail_slot.append({
                "start_time":str(i.schedule.start_time),
                "id":i.schedule.id
            })
    slot = [{
        "weekday": weekday,
        "time": i["start_time"],
        "schedule_id": i["id"]
    } for i in avail_slot]
    payload = {
        "lab_id": lab_id,
        "date": dt,
        "test": data['test'],
        "slot": slot
    }
    session.close()
    connection.close()
    return make_response("success", HTTPStatus.Success, payload)


@appointmentBP.route('/book_lab_slot', methods=['POST'])
def booklabslot():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)

    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("User not verified", HTTPStatus.UnAuthorised)
    data = request.json
    required_parameters = ['lab_id', 'test', 'date', 'schedule_id']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    lab_id = data['lab_id']
    schedule_id = data['schedule_id']
    dt = data['date']
    year, month, day = (int(x) for x in dt.split('-'))    
    date = datetime.date(year, month, day)
    weekday = date.strftime("%A")
    weekday = weekday.lower()
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    schedule_q = session.query(Schedule).filter_by(id=schedule_id).first()
    price_q = session.query(LabTest).join(TestAvail).filter(TestAvail.testname==data['test']).filter(LabTest.diagnoselab_id==lab_id).first()
    bk = Appointment(
        date_created = date,
        start_time = schedule_q.start_time,
        aggenda = data['test'],
        price = price_q.price,
        diagnoselab_id = lab_id
    )
    booking = {
        "date_created": date,
        "start_time":str(schedule_q.start_time),
        "aggenda": data['test'],
        "price": price_q.price,
        "diagnoselab_id": lab_id,
        "user_id": usr_.id
    }
    session.add(bk)
    session.commit()
    session.close()
    connection.close()
    return make_response("success", HTTPStatus.Success, booking)
